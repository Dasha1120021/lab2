<footer class=" container pt-4 my-md-5 pt-md-5 border-top">
    <div class="row">
        <div class="col-12 col-md">
            <img class="mb-2" src="/docs/5.0/assets/brand/bootstrap-logo.svg" alt="" width="24" height="19">
            <small class="d-block mb-3 text-muted"><ya-tr-span data-index="35-0" data-translated="true" data-source-lang="en" data-target-lang="ru" data-value="© 2017–2021" data-translation="© 2017–2021" data-type="trSpan">© 2017–2021</ya-tr-span></small>
        </div>
        <div class="col-6 col-md">
            <h5><ya-tr-span data-index="36-0" data-translated="true" data-source-lang="en" data-target-lang="ru" data-value="Features" data-translation="Характеристики" data-type="trSpan">Характеристики</ya-tr-span></h5>
            <ul class="list-unstyled text-small">
                <li class="mb-1"><a class="link-secondary text-decoration-none" href="#"><ya-tr-span data-index="37-0" data-translated="true" data-source-lang="en" data-target-lang="ru" data-value="Cool stuff" data-translation="Классная штука" data-type="trSpan">Классная штука</ya-tr-span></a></li>
                <li class="mb-1"><a class="link-secondary text-decoration-none" href="#"><ya-tr-span data-index="38-0" data-translated="true" data-source-lang="en" data-target-lang="ru" data-value="Random feature" data-translation="Случайная функция" data-type="trSpan">Случайная функция</ya-tr-span></a></li>
                <li class="mb-1"><a class="link-secondary text-decoration-none" href="#"><ya-tr-span data-index="39-0" data-translated="true" data-source-lang="en" data-target-lang="ru" data-value="Team feature" data-translation="Командная функция" data-type="trSpan">Командная функция</ya-tr-span></a></li>
                <li class="mb-1"><a class="link-secondary text-decoration-none" href="#"><ya-tr-span data-index="40-0" data-translated="true" data-source-lang="en" data-target-lang="ru" data-value="Stuff for developers" data-translation="Материал для разработчиков" data-type="trSpan">Материал для разработчиков</ya-tr-span></a></li>
                <li class="mb-1"><a class="link-secondary text-decoration-none" href="#"><ya-tr-span data-index="41-0" data-translated="true" data-source-lang="en" data-target-lang="ru" data-value="Another one" data-translation="Другой" data-type="trSpan">Другой</ya-tr-span></a></li>
                <li class="mb-1"><a class="link-secondary text-decoration-none" href="#"><ya-tr-span data-index="42-0" data-translated="true" data-source-lang="en" data-target-lang="ru" data-value="Last time" data-translation="Последний раз" data-type="trSpan">Последний раз</ya-tr-span></a></li>
            </ul>
        </div>
        <div class="col-6 col-md">
            <h5><ya-tr-span data-index="43-0" data-translated="true" data-source-lang="en" data-target-lang="ru" data-value="Resources" data-translation="Ресурсы" data-type="trSpan">Ресурсы</ya-tr-span></h5>
            <ul class="list-unstyled text-small">
                <li class="mb-1"><a class="link-secondary text-decoration-none" href="#"><ya-tr-span data-index="44-0" data-translated="true" data-source-lang="en" data-target-lang="ru" data-value="Resource" data-translation="Ресурс" data-type="trSpan">Ресурс</ya-tr-span></a></li>
                <li class="mb-1"><a class="link-secondary text-decoration-none" href="#"><ya-tr-span data-index="45-0" data-translated="true" data-source-lang="en" data-target-lang="ru" data-value="Resource name" data-translation="Имя ресурса" data-type="trSpan">Имя ресурса</ya-tr-span></a></li>
                <li class="mb-1"><a class="link-secondary text-decoration-none" href="#"><ya-tr-span data-index="46-0" data-translated="true" data-source-lang="en" data-target-lang="ru" data-value="Another resource" data-translation="Еще один ресурс" data-type="trSpan">Еще один ресурс</ya-tr-span></a></li>
                <li class="mb-1"><a class="link-secondary text-decoration-none" href="#"><ya-tr-span data-index="47-0" data-translated="true" data-source-lang="en" data-target-lang="ru" data-value="Final resource" data-translation="Конечный ресурс" data-type="trSpan">Конечный ресурс</ya-tr-span></a></li>
            </ul>
        </div>
        <div class="col-6 col-md">
            <h5><ya-tr-span data-index="48-0" data-translated="true" data-source-lang="en" data-target-lang="ru" data-value="About" data-translation="О нас" data-type="trSpan">О нас</ya-tr-span></h5>
            <ul class="list-unstyled text-small">
                <li class="mb-1"><a class="link-secondary text-decoration-none" href="#"><ya-tr-span data-index="49-0" data-translated="true" data-source-lang="en" data-target-lang="ru" data-value="Team" data-translation="Команда" data-type="trSpan">Команда</ya-tr-span></a></li>
                <li class="mb-1"><a class="link-secondary text-decoration-none" href="#"><ya-tr-span data-index="50-0" data-translated="true" data-source-lang="en" data-target-lang="ru" data-value="Locations" data-translation="Адреса" data-type="trSpan">Адреса</ya-tr-span></a></li>
                <li class="mb-1"><a class="link-secondary text-decoration-none" href="#"><ya-tr-span data-index="51-0" data-translated="true" data-source-lang="en" data-target-lang="ru" data-value="Privacy" data-translation="Конфиденциальность" data-type="trSpan">Конфиденциальность</ya-tr-span></a></li>
                <li class="mb-1"><a class="link-secondary text-decoration-none" href="#"><ya-tr-span data-index="52-0" data-translated="true" data-source-lang="en" data-target-lang="ru" data-value="Terms" data-translation="Условия" data-type="trSpan">Условия</ya-tr-span></a></li>
            </ul>
        </div>
    </div>
</footer>
