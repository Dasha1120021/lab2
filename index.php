 <!DOCTYPE html>
 <html lang="ru">
 <head>
     <meta charset="UTF-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <meta http-equiv="X-UA-compatible" content="ie-edge">
     <link rel="stylesheet" href="css/style.css">
     <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css">
          <title>Document</title>
 </head>
 <body>
 <?php  require "bloks/header.php"?>
     <div class="container mt-5">
         <h3> Мой блог</h3>
     </div>
     <main class="container">
         <div class="p-4 p-md-5 mb-4 text-white rounded bg-dark">
             <div class="col-md-6 px-0">
                 <h1 class="display-4 fst-italic">Как не сгореть на работе</h1>
                 <p class="lead my-3">Что делать, если некогда любимая работа превратилась в каторгу?</p>
                 <p class="lead mb-0"><a href="/karera.php" class="text-white fw-bold">Читать дальше...</a></p>
             </div>
         </div>

         <div class="row mb-2">
             <div class="col-md-6">
                 <div class="row g-0 border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
                     <div class="col p-4 d-flex flex-column position-static">
                         <strong class="d-inline-block mb-2 text-primary">Красота</strong>
                         <h3 class="mb-0">Как начать заботиться о себе: 2 простых идеи</h3>
                         <div class="mb-1 text-muted">Nov 12</div>
                         <p class="card-text mb-auto">Прошедший год изменил наше отношение к себе. В постоянно меняющемся мире мы познавали дзен в регулярных медитациях... </p>
                         <a href="/krasota.php" class="stretched-link">Читать дальше</a>
                     </div>
                     <div class="col-auto d-none d-lg-block">
                        <img src="img/111.jpg">
                     </div>
                 </div>
             </div>
             <div class="col-md-6">
                 <div class="row g-0 border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
                     <div class="col p-4 d-flex flex-column position-static">
                         <strong class="d-inline-block mb-2 text-success">Здоровье</strong>
                         <h3 class="mb-0">Пища для размышлений: как еда влияет на наш мозг</h3>
                         <div class="mb-1 text-muted">Nov 11</div>
                         <p class="mb-auto">Мозг контролирует каждый аспект нашей жизни — мысли, движения, чувства. Для этого ему постоянно требуется энергия, которую мы получаем из пищи. </p>
                         <a href="/zdorove.php" class="stretched-link">Читать дальше</a>
                     </div>
                     <div class="col-auto d-none d-lg-block">
                        <img src="img/112.jpg">
                     </div>
                 </div>
             </div>
         </div>
     </main>
 </div>
 <?php  require "bloks/footer.php"?>
 </body>
 </html>
