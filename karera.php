<!DOCTYPE html>
<html lang="ru" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-compatible" content="ie-edge">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css">
    <title>Карьера</title>
</head>
<body>
<?php  require "bloks/header.php"?>


<main>
    <div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-center bg-light">
        <div class="col-md-5 p-lg-5 mx-auto my-5">
            <h1 class="display-4 fw-normal">Как не сгореть на работе</h1>
            <p class="lead fw-normal"> Что делать, если некогда любимая работа превратилась в каторгу?
                ПУТИ К СПАСЕНИЮ </p>
        </div>
        <div class="product-device shadow-sm d-none d-md-block"></div>
        <div class="product-device product-device-2 shadow-sm d-none d-md-block"></div>
    </div>
    <div class="d-md-flex flex-md-equal w-100 my-md-3 ps-md-3">
        <div class="bg-light me-md-3 pt-3 px-3 pt-md-5 px-md-5 text-center overflow-hidden">
            <div class="my-3 p-3">
                <h2 class="display-5">1. Элементарная усталость</h2>
                <p class="lead"> Человек не хочет работать не потому, что обленился или разочаровался в своей деятельности, а просто как в анекдоте про девочку, которая не курит и не пьет: «Не могу больше...». Надо признать, что многие из нас оскал капитализма видят каждый день, причем с 9 утра до 9 вечера. Объем работы подчас бывает такой, что его физически выполнить невозможно!

                    Решение: следите за интенсивностью своего труда. Вы не биоробот. Вы не можете и не должны работать сутками. И вы в состоянии донести это до окружающих, особенно до начальства.</p>
            </div>

        </div>
        <div class="bg-light me-md-3 pt-3 px-3 pt-md-5 px-md-5 text-center overflow-hidden">
            <div class="my-3 py-3">
                <h2 class="display-5">2. Масса других проблем вне работы</h2>
                <p class="lead">Какой бы вы ни вели образ жизни, у вас есть другие сферы, где приходится много трудиться: дом, семья, ребенок. Те немногие силы, что остаются после работы, уходят туда.

                    Решение: надо смириться с временным выгоранием, затаиться и просто пережить этот период. Работайте по минимуму, не надрываясь, а сконцентрируйтесь на проблемах из той, «другой» жизни.</p>
            </div>

        </div>
    </div>

</main>

<?php  require "bloks/footer.php"?>
</body>
</html>