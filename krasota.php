<!DOCTYPE html>
<html lang="ru" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-compatible" content="ie-edge">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css">
    <title>Красота</title>
</head>
<body>
<?php  require "bloks/header.php"?>

<main>
    <div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-center bg-light">
        <div class="col-md-5 p-lg-5 mx-auto my-5">
            <h1 class="display-4 fw-normal">Как начать заботиться о себе: 2 простых идеи</h1>
            <p class="lead fw-normal">Забота о себе — новая норма</p>
        </div>
        <div class="product-device shadow-sm d-none d-md-block"></div>
        <div class="product-device product-device-2 shadow-sm d-none d-md-block"></div>
    </div>
    <div class="d-md-flex flex-md-equal w-100 my-md-3 ps-md-3">
        <div class="bg-light me-md-3 pt-3 px-3 pt-md-5 px-md-5 text-center overflow-hidden">
            <div class="my-3 p-3">
                <h2 class="display-5">ИДЕЯ 1 — Массаж</h2>
                <p class="lead">В конце тяжелого дня выделите час на массаж. Ваши спина и шея будут вам благодарны. Массаж может улучшить ваш сон, нивелировать стресс и тревогу, восстановить тело после активных тренировок, расслабить и просто доставить удовольствие.</p>
            </div>

        </div>
        <div class="bg-light me-md-3 pt-3 px-3 pt-md-5 px-md-5 text-center overflow-hidden">
            <div class="my-3 py-3">
                <h2 class="display-5">ИДЕЯ 2 — Йога</h2>
                <p class="lead">Хатха или дживамукти-йога подойдут для того, чтобы дать необходимую для тонуса нагрузку телу, но не дадут оставить в зале последние силы. Занятие йогой разомнет все мышцы и суставы, поможет найти гармонию, и вообще, что может быть лучше шавасаны под одеялом в конце практики.</p>
            </div>

        </div>
    </div>

</main>

<?php  require "bloks/footer.php"?>
</body>
</html>
