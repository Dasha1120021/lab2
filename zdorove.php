<!DOCTYPE html>
<html lang="ru" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-compatible" content="ie-edge">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css">
    <title>Здоровье</title>
</head>
<body>
<?php  require "bloks/header.php"?>

<main>
    <div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-center bg-light">
        <div class="col-md-5 p-lg-5 mx-auto my-5">
            <h1 class="display-4 fw-normal">Пища для размышлений: как еда влияет на наш мозг</h1>
            <p class="lead fw-normal">Мы то, что мы едим</p>
        </div>
        <div class="product-device shadow-sm d-none d-md-block"></div>
        <div class="product-device product-device-2 shadow-sm d-none d-md-block"></div>
    </div>
    <div class="d-md-flex flex-md-equal w-100 my-md-3 ps-md-3">
        <div class="bg-light me-md-3 pt-3 px-3 pt-md-5 px-md-5 text-center overflow-hidden">
            <div class="my-3 p-3">
                <h2 class="display-5">Микробиом</h2>
                <p class="lead">Микробиом состоит из огромного количества бактерий, находящихся в здоровом кишечнике. Для мозга эти кишечные бактерии важны. Они защищают слизистую оболочку кишечника и обеспечивают прочный барьер против токсинов и «плохих» бактерий, а также активируют нервные пути, которые проходят непосредственно между кишечником и мозгом. </p>
            </div>

        </div>
        <div class="bg-light me-md-3 pt-3 px-3 pt-md-5 px-md-5 text-center overflow-hidden">
            <div class="my-3 py-3">
                <h2 class="display-5">Много не значит хорошо</h2>
                <p class="lead">Многие из нас убеждены, что от голода можно умереть, поэтому, не задумываясь, инстинктивно мы пытаемся подавить чувство голода как можно быстрее. Однако краткосрочное голодание не вредит, а, напротив, укрепляет жизненные силы человека намного больше, чем переедание. Мозг любит кетоны, возникающие в результате метаболизма жира, — повышается работоспособность, скорость реакций и креативность.</p>
            </div>

        </div>
    </div>

</main>

<?php  require "bloks/footer.php"?>
</body>
</html>
